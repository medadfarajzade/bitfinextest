﻿using Auctioning.API;
using Auctioning.Models.Entities;
using NetCoreServer;
using System.Net.Sockets;
using System.Net;
using Serilog;
using Auctioning.Server;

namespace Auctioning.RpcServer
{
    // got this from NetCoreServer docs
    public class JRpcServer : TcpServer
    {
        private readonly MessageManager _messageManager;
        private readonly BitfinexContext _context;
        private readonly ILogger _logger;
        private readonly Devices _device; 
        public JRpcServer(Devices device, MessageManager messageManager, BitfinexContext context, ILogger logger, IPAddress address, int port) : base(address, port)
        {
            _messageManager = messageManager;
            _context = context;
            _logger = logger;
            _device = device;
        }

        protected override TcpSession CreateSession()
        {
            var session = new JRpcSession(this, _device, _context, _messageManager, _logger);
            return session; 
        }

        protected override void OnError(SocketError error)
        {
            Console.WriteLine($"TCP server caught an error with code {error}");
        }
    }
}
