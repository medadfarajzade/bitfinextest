﻿using Serilog;
using System.Text;
using Auctioning.API;
using Auctioning.Models.Entities;
using NetCoreServer;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace Auctioning.Server
{
    public class JRpcSession : TcpSession
    {
        private readonly MessageManager _messageManager;
        private readonly ILogger _logger;
        private readonly BitfinexContext _context;
        private Devices _device; 
        public JRpcSession(TcpServer server, Devices device, BitfinexContext context, MessageManager messageManager, ILogger logger) : base(server)
        {
            _messageManager = messageManager;
            _logger = logger;
            _context = context;
            _device = device; 
        }

        protected override async void OnConnected()
        {
            var connectedUser = new ConnectedUser
            {
                DeviceId = _device.Id,
                SocketId = Id,
            };
            
            await _context.Users.AddAsync(connectedUser);
            await _context.SaveChangesAsync(); 
        }
        protected override async void OnDisconnected()
        {
            var userDelete = await _context.Users.FirstOrDefaultAsync(x => x.SocketId == Id);
            if (userDelete != null)
            {
                var deviceDelete = await _context.Devices.FirstOrDefaultAsync(x => x.Id == userDelete.DeviceId);
                if (deviceDelete != null)
                {
                    _context.Users.Remove(userDelete);
                    _context.Devices.Remove(deviceDelete);
                    await _context.SaveChangesAsync();
                }
            }
        }

        protected override async void OnReceived(byte[] buffer, long offset, long size)
        {
            try
            {
                Console.WriteLine("RpcServer.OnRecieved");
                string message = Encoding.UTF8.GetString(buffer, (int)offset, (int)size);
                Console.WriteLine(message);

                var convertedToJson = JsonConvert.SerializeObject(message);
                JObject requestObject = JObject.Parse(convertedToJson);
                if (requestObject != null)
                {
                    string methodName = requestObject["method"].ToString();
                    int key = int.Parse(methodName);
                    string content = requestObject["params"].ToString();

                    if (methodName != null && content != null)
                    {
                        var user = await _context.Users.FirstOrDefaultAsync(x => x.SocketId == Id);
                        if (user == null)
                            throw new ArgumentNullException(nameof(user));

                        await _messageManager.TryExecuteMessage(key, content, user.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
