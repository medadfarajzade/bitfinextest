﻿using Auctioning.API;
using Auctioning.Models.Entities;
using Auctioning.Models.Input;
using Auctioning.RpcServer;
using Newtonsoft.Json;

namespace Auctioning.Communication.Packets.Incoming
{
    public class PlaceItemSaleIncomingPacket : IIncomingPacket
    {
        private readonly IAuctionService _auctionService; 
        public int HeaderId => 0x63;

        public PlaceItemSaleIncomingPacket(IAuctionService auctionService)
        {
            _auctionService = auctionService;
        }
        public async ValueTask Recieve(string message, Guid userId)
        {
            var jsonSerialize = JsonConvert.DeserializeObject<SellingItemInput>(message);
            if (jsonSerialize != null)
                await using (var auctionService = _auctionService)
                {
                    await auctionService.SellItem(jsonSerialize, userId);
                }

        }

    }
}
