﻿using Auctioning.API;
using Auctioning.Models.Entities;
using Auctioning.RpcClient;
using Auctioning.RpcServer;

namespace Auctioning.Communication.Packets.Outgoing
{
    public class PlaceItemSaleOutgoingNotificationPacket : IOutgoingPacket<SellingItem>
    {
        public int HeaderId => 0x62; 
        public Guid UserId { get; set; }

        public PlaceItemSaleOutgoingNotificationPacket()
        {

        }

        public ValueTask Send(string message)
        {
            // not completed...
            Settings.Server.Multicast(message); 
            return ValueTask.CompletedTask; 
        }

    }
}
