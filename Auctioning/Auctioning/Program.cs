﻿using Auctioning;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console().CreateLogger();


IServiceCollection serviceCollection = new ServiceCollection();
serviceCollection.RegisterDependencies();
IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
var server = serviceProvider.GetService<BitFinexServer>();
if (server != null)
    server.Start();
else
    throw new ArgumentNullException("Server dependency cannot be null!"); 