﻿using Auctioning.API;
using Auctioning.Communication.Packets.Incoming;
using Auctioning.Communication.Packets.Outgoing;
using Auctioning.Logic.Auction;
using Auctioning.Logic.Bidding;
using Auctioning.Logic.Users;
using Auctioning.Models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Auctioning
{
    public static class DependencyUtils
    {
        public static void RegisterDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IIncomingPacket, PlaceItemSaleIncomingPacket>();
            serviceCollection.AddTransient<IOutgoingPacket<SellingItem>, PlaceItemSaleOutgoingNotificationPacket>();
            serviceCollection.AddSingleton<MessageManager>();
            serviceCollection.AddSingleton<BitFinexServer>();
            serviceCollection.AddSingleton<ILogger>(x => Log.Logger);



            serviceCollection.AddTransient<IAuctionService, AuctionService>();
            serviceCollection.AddTransient<IBiddingService, BiddingService>(); 
            serviceCollection.AddTransient<IUserService, UserService>();
            serviceCollection.AddDbContext<BitfinexContext>(); 
            //serviceCollection.AddSingleton<IThreadService, ThreadService>(); 
        }
    }
}
