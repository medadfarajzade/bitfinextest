﻿using Auctioning.API;
using Auctioning.Models.Entities;
using Auctioning.RpcClient;
using Auctioning.RpcServer;
using Serilog;
using System.Net;

namespace Auctioning
{
    public class BitFinexServer
    {
        private readonly BitfinexContext _context;
        private readonly MessageManager _messageManager;
        private readonly ILogger _logger;
        public BitFinexServer(BitfinexContext context, MessageManager messageManager, ILogger logger) 
        { ;
            _context = context; 
            _messageManager = messageManager;
            _logger = logger;
        }

        public async void Start()
        {
            try
            {
                var random = new Random();
                var portNumber = random.Next(1000, 9999);
                var ip = "127.0.0.1";
                _context.Database.EnsureCreated();
                var checkDevices = _context.Devices.Any(x => x.IP == ip && x.Port == portNumber);
                if (checkDevices)
                    Start();

                var device = new Devices { IP = ip, Port = portNumber };
                _context.Devices.Add(device);
                await _context.SaveChangesAsync();


                var threadServer = new Thread(() =>
                {
                    var server = new JRpcServer(device, _messageManager, _context, _logger, IPAddress.Parse(device.IP), device.Port);
                    Settings.Server = server; 
                    server.Start();
                });
                var threadClient = new Thread(() =>
                {
                    var client = new JRpcClient(_context, _messageManager, device.IP, device.Port);
                    client.ConnectAsync();
                    Console.ReadKey();
                }); 
                threadServer.Start(); 
                threadClient.Start();

    

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message); 
            }
        }
    }
}
