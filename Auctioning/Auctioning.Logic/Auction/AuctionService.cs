﻿using Auctioning.API;
using Auctioning.Logic.Users;
using Auctioning.Models.Entities;
using Auctioning.Models.Input;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Auctioning.Logic.Auction
{
    public class AuctionService : IAuctionService, IAsyncDisposable
    {
        private readonly BitfinexContext _context; 
        private readonly IOutgoingPacket<SellingItem> _outgoingPacket;  
        public AuctionService(BitfinexContext context, IOutgoingPacket<SellingItem> outgoingPacket)
        {
            _context = context;
            _outgoingPacket = outgoingPacket;
        }

        public IEnumerable<SellingItem> GetSellingItems(int skip, int take)
        {
            var items = _context.SellingItems.Skip(skip).Take(take);
            return items;
        }
        public async ValueTask<SellingItem> SellItem(SellingItemInput item, Guid userId)
        {
            try
            {
                if (item == null || userId == Guid.Empty)
                    throw new ArgumentNullException("user or item was null"); // example message...
                var getUser = await _context.Users.FirstOrDefaultAsync(x => x.Id == userId);
                if (getUser == null)
                    throw new ArgumentNullException(nameof(getUser)); 
                var username = getUser.Name;

                var sellingItem = new SellingItem(); 

                // use automapper for the mapping...
                sellingItem.ItemName = item.ItemName;
                sellingItem.MinimumPrice = item.MinimumPrice;
                sellingItem.SellingUser = getUser.Name;
                sellingItem.TimeUntilExpiry = DateTime.UtcNow.AddMinutes(15); 
                await _context.SellingItems.AddAsync(sellingItem);
                await _context.SaveChangesAsync();

                var convertedSellingItem = JsonConvert.SerializeObject(sellingItem);
                await _outgoingPacket.Send(convertedSellingItem); 
                return sellingItem;
            }
            catch
            {
                throw;
            }
        }

        public ValueTask DisposeAsync()
        {
            // simple calling GC for now. 
            GC.SuppressFinalize(this);
            return ValueTask.CompletedTask;
        }
    }
}
