﻿using Auctioning.API;
using Auctioning.Logic.Auction;
using Auctioning.Logic.Users;
using Auctioning.Models.Entities;
using Auctioning.Models.Input;
using Microsoft.EntityFrameworkCore;

namespace Auctioning.Logic.Bidding
{
    public class BiddingService : IBiddingService, IAsyncDisposable
    {
        private readonly BitfinexContext _context; 
        public BiddingService(BitfinexContext context)
        {
            _context = context; 
        }

        public ValueTask DisposeAsync()
        {
            // simple calling GC for now. 
            GC.SuppressFinalize(this);
            return ValueTask.CompletedTask;
        }

        // will be called from the cycle...
        public async ValueTask WinBid()
        {
            var getItems = _context.SellingItems.Include(x => x.PlacedBids).ThenInclude(x => x.User).Where(x => x.TimeUntilExpiry >= DateTime.UtcNow);
            if (getItems.Any())
            {
                foreach (var item in getItems)
                {
                    var highestBid = item.PlacedBids.OrderByDescending(x => x.Price).FirstOrDefault();
                    if (highestBid != null)
                    {
                        Console.WriteLine($"Winner: {highestBid.User.Name}, Bid Amount: {highestBid.Price}, for item: {item.ItemName}");
                    }

                    _context.SellingItems.Remove(item);
                    await _context.SaveChangesAsync();

                }
            }
        }

        // called from incoming packet.
        public async ValueTask<PlacedBid> PlaceBid(PlaceBidInput input)
        {
            try
            {
                if (input == null)
                    throw new ArgumentNullException(nameof(input));

                var getUser = await _context.Users.FirstOrDefaultAsync(x => x.Id == input.UserId);
                if (getUser == null)
                    throw new ArgumentNullException();
                var getItem = await _context.SellingItems.FirstOrDefaultAsync(x => x.Id == input.ItemId);
                if (getItem == null)
                    throw new ArgumentNullException();

                var placedBid = new PlacedBid();
                // automapper here. 
                placedBid.User = getUser; 
                placedBid.Item = getItem;
                placedBid.Price = input.Price;
                await _context.AddAsync(placedBid);
                await _context.SaveChangesAsync(); 

                // should send out outgoing packet here. 
                return placedBid;
            }
            catch
            {
                throw;
            }




        }
    }
}
