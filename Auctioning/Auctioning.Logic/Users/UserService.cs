﻿using Auctioning.API;
using Auctioning.Models.Entities;
using Newtonsoft.Json;

namespace Auctioning.Logic.Users
{
    public class UserService : IUserService, IAsyncDisposable
    {
        private readonly BitfinexContext _context; 

        // should save into context.
        public UserService(BitfinexContext context) 
        {
            _context = context; 
        }

        [Obsolete]
        public Guid Connect(ConnectedUser user)
        {

            // moved into socket...
            return Guid.Empty; 
        }

        public ValueTask DisposeAsync()
        {
            // simple calling GC for now. 
            GC.SuppressFinalize(this);
            return ValueTask.CompletedTask;
        }
    }
}
