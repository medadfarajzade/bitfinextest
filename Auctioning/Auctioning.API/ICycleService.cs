﻿namespace Auctioning.Logic.Threads
{
    public interface ICycleService
    {
        ValueTask Run();
        ValueTask DisposeAsync(); 
    }
}
