﻿

using Auctioning.Models.Entities;

namespace Auctioning.API
{
    public interface IOutgoingPacket<T> where T : class
    {
        int HeaderId { get; }
        Guid UserId { get; }
        ValueTask Send(string message);
    }
}
