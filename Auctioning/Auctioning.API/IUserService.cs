﻿using Auctioning.Models.Entities;

namespace Auctioning.API
{
    public interface IUserService
    {
        Guid Connect(ConnectedUser user);
        ValueTask DisposeAsync(); 

    }
}
