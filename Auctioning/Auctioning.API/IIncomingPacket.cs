﻿namespace Auctioning.API
{
    public interface IIncomingPacket
    {
        int HeaderId { get; }
        ValueTask Recieve(string message, Guid userId);
    }
}
