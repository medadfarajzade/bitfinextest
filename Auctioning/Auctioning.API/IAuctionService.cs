﻿using Auctioning.Models.Entities;
using Auctioning.Models.Input;
using System.Threading.Tasks;

namespace Auctioning.API
{
    public interface IAuctionService
    {
        ValueTask DisposeAsync();
        ValueTask<SellingItem> SellItem(SellingItemInput input, Guid userId);
        IEnumerable<SellingItem> GetSellingItems(int skip, int take);
    }
}
