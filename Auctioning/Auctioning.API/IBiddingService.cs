﻿using Auctioning.Models.Entities;
using Auctioning.Models.Input;

namespace Auctioning.API
{
    public interface IBiddingService
    {
        ValueTask<PlacedBid> PlaceBid(PlaceBidInput input);
        ValueTask WinBid();
        ValueTask DisposeAsync(); 

    }
}
