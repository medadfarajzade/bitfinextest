﻿using System.Collections.Concurrent;
using Serilog; 
using System.Text;

namespace Auctioning.API;


public class MessageManager
{
    private readonly Dictionary<int, IIncomingPacket> _incomingMessages;
    private readonly ILogger _logger;
    private readonly ConcurrentDictionary<int, Task> _runningTasks;
    private readonly TaskFactory _eventDispatcher;
    private readonly int _maximumRunTimeInSec = 300;

    public MessageManager(IEnumerable<IIncomingPacket> messages, ILogger logger)
    {
        _incomingMessages = messages.ToDictionary(x => x.HeaderId, x => x);
        _eventDispatcher = new TaskFactory(TaskCreationOptions.PreferFairness, TaskContinuationOptions.None);
        _runningTasks = new ConcurrentDictionary<int, Task>();
        _logger = logger;
    }

    public string GetMessageHeader(IDictionary<string, object> headers, string key)
    {
        if (headers != null && headers.TryGetValue(key, out object headerObject))
        {
            if (headerObject is byte[] bytes)
            {
                return Encoding.UTF8.GetString(bytes);
            }
        }
        return null;
    }

    public Task TryExecuteMessage(int Key, string message, Guid userId)
    {

        if (!_incomingMessages.TryGetValue(Key, out IIncomingPacket mes))
        {
            _logger.Warning("Unhandled queue: " + mes);
            return Task.CompletedTask;
        }

        _logger.Information("Handled queue: [" + mes.HeaderId + "] " + mes.GetType().Name);
        ExecutePacketAsync(mes, message, userId);
        return Task.CompletedTask;

    }

    public void ExecutePacketAsync(IIncomingPacket queue, string message, Guid userId)
    {
        CancellationTokenSource cancelToken = new CancellationTokenSource();
        CancellationToken token = cancelToken.Token;
        Task t = _eventDispatcher.StartNew(async () =>
        {
            await queue.Recieve(message, userId);
            token.ThrowIfCancellationRequested();
        }, token);


        _runningTasks.TryAdd(t.Id, t);
        try
        {
            if (!t.Wait(_maximumRunTimeInSec * 1000, token))
            {
                cancelToken.Cancel();
            }
        }
        catch (AggregateException ex)
        {
            foreach (Exception e in ex.Flatten().InnerExceptions)
            {
                throw e;
            }
        }
        catch (OperationCanceledException)
        {

        }
        finally
        {
            _runningTasks.TryRemove(t.Id, out Task _);
            cancelToken.Dispose();
        }
    }
    public void WaitForAllToComplete()
    {
        foreach (Task t in _runningTasks.Values.ToList())
        {
            t.Wait();
        }
    }
    public void UnregisterAll()
    {
        _incomingMessages.Clear();
    }
}
