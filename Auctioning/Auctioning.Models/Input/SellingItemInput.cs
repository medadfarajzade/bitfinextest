﻿namespace Auctioning.Models.Input
{
    public class SellingItemInput
    {
        public string ItemName { get; set; }
        public double MinimumPrice { get; set; }
    }
}
