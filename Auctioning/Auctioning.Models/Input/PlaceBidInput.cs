﻿namespace Auctioning.Models.Input
{
    public class PlaceBidInput
    {
        public Guid UserId { get; set; } 
        public Guid ItemId { get; set; } 
        public double Price { get; set; } 
    }
}
