﻿namespace Auctioning.Models.Entities;
public class ConnectedUser
{
    public Guid Id { get; set; } 
    public string Name { get; set; } 
    public Guid SocketId { get; set; } 
    public Guid DeviceId { get; set; } 
}
