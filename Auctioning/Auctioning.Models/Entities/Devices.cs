﻿namespace Auctioning.Models.Entities
{
    public class Devices
    {
        public Guid Id { get; set; } 
        public string IP { get; set; } 
        public int Port { get; set; } 
    }
}
