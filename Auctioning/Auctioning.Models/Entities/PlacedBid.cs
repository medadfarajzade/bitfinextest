﻿namespace Auctioning.Models.Entities
{
    public class PlacedBid
    {
        public Guid Id { get; set; } 
        public ConnectedUser User { get; set; }
        public SellingItem Item{ get; set; }
        public double Price { get; set; }
    }
}
