﻿namespace Auctioning.Models.Entities
{
    public class SellingItem
    {
        public Guid Id { get; set; } 
        public string ItemName { get; set; }
        public double MinimumPrice { get; set; }
        public string SellingUser { get; set; }
        public DateTime TimeUntilExpiry { get; set; }
        public List<PlacedBid> PlacedBids { get; set; } 
    }
}
