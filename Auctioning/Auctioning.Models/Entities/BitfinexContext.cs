﻿using Microsoft.EntityFrameworkCore;

namespace Auctioning.Models.Entities
{
    public class BitfinexContext : DbContext
    {
        public DbSet<SellingItem> SellingItems { get; set; } 
        public DbSet<ConnectedUser> Users { get; set; } 
        public DbSet<Devices> Devices { get; set; } 
        public DbSet<PlacedBid> PlacedBids { get; set; } 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //SQLitePCL.Batteries.Init(); // Initialize SQLite provider
            optionsBuilder.UseSqlite("Data Source=mydatabase.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SellingItem>(entity =>
            {
                entity.ToTable("selling_items");
                entity.Property(e => e.Id).HasColumnName("id").HasDefaultValueSql("uuid_generate_v1()");
                entity.HasMany(x => x.PlacedBids);
            }); 
        }
    }
}
