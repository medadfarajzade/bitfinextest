﻿

using Auctioning.API;
using Auctioning.Models.Entities;
using Microsoft.EntityFrameworkCore;
using NetCoreServer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Auctioning.RpcClient
{
    public class JRpcClient : TcpClient
    {
        private readonly MessageManager _messageManager;
        private readonly BitfinexContext _context; 
        public JRpcClient(BitfinexContext context, MessageManager messageManager, string address, int port) : base(address, port)
        {
            _messageManager = messageManager;
            _context = context; 
        }

        public void DisconnectAndStop()
        {
            _stop = true;
            DisconnectAsync();
            while (IsConnected)
                Thread.Yield();
        }
        protected override void OnConnected()
        {
            Console.WriteLine($"client connected a new session with Id {Id}");
        }

        protected override void OnDisconnected()
        {

        }
        protected override async void OnReceived(byte[] buffer, long offset, long size)
        {

        }


        protected override void OnError(System.Net.Sockets.SocketError error)
        {

        }

        private bool _stop;

        // move out from this method. 
        private JObject ConstructOutgoingMessage(int headerId, string incoming)
        {
            JObject jsonRpcRequest = new JObject
            {
                ["jsonrpc"] = "2.0",
                ["method"] = headerId,
                ["params"] = new JObject
                {
                    ["message"] = incoming
                },
                ["id"] = 1
            };

            return jsonRpcRequest;
        }
    }
}
           
